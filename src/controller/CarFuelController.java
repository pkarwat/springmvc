package controller;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import model.CarFuelDTO;
import model.GreetingDTO;
import model.ServerResponseDTO;
import model.ServerResponseListDTO;
import service.CarFuelService;

@Controller
@RequestMapping("/car-fuel")

public class CarFuelController {

	private static final String TAG = CarFuelController.class.getName();	
	private static final Logger log = Logger.getLogger(CarFuelController.class.getName());
	
	// --- Members -------------------------------------------------------------------------------------------
	
	private final AtomicLong counter = new AtomicLong();
	
	// --- Methods -------------------------------------------------------------------------------------------
	
	@Autowired
    CarFuelService carFuelServiceImpl;   	
    
    @RequestMapping(method=RequestMethod.POST)
    public @ResponseBody ServerResponseDTO<Long> carFuelPost(@RequestBody CarFuelDTO c) {
    	log.info(TAG + " POST " + c.toString());
    	
    	//add validation here
    	
    	long id = carFuelServiceImpl.add(c);
    	
    	ServerResponseDTO<Long> response = new ServerResponseDTO<Long>();
    	response.setCode(ServerResponseDTO.RESPONSE_CODE.SUCCESS.ordinal());
    	response.setMsg("Dodano nowy wpis paliwa. Id: " + id);
    	response.setItem(id);    	
    	
    	return response;
    }
    
	@RequestMapping("/{id}")
    //public @ResponseBody GreetingDTO carFuelGet(@RequestParam(value="name", required=false, defaultValue="Stranger") String name) {
	public @ResponseBody GreetingDTO carFuelGet(@PathVariable int id) {
		log.info(TAG + " GET --> " + id);				
		return new GreetingDTO(counter.incrementAndGet(), String.format("CarFuelController GET " + id, id));
    }
	
	//TODO getLast
	//TODO update
	//TODO delete
	//TODO getAll
	
	@RequestMapping("/list/page/{page}")
	public @ResponseBody ServerResponseListDTO<CarFuelDTO> carFuelGetAll(@PathVariable int page) {
		log.info(TAG + " GET list, page --> " + page);
		
		ArrayList<CarFuelDTO> list = (ArrayList<CarFuelDTO>) carFuelServiceImpl.getAll(page);
		
		ServerResponseListDTO<CarFuelDTO> response = new ServerResponseListDTO<>();
		response.setCode(ServerResponseDTO.RESPONSE_CODE.SUCCESS.ordinal());
		response.setItems(list);
		response.setMsg("Pobrano list�");
		
		return response;
	}	
	
	//TODO getAll(userId)
		
}
