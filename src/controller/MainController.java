package controller;

import java.util.Date;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import model.CarFuelDTO;
import model.UserDTO;
import service.CarFuelService;
import service.CommonService;

@Controller
public class MainController {

	private static final String TAG = MainController.class.getName();
	private static final Logger log = Logger.getLogger(MainController.class.getName());
		
	@RequestMapping("/welcome")
	public ModelAndView processWelcome() {
		log.info(TAG + " /welcome");				

		String message = "<br><div style='text-align:center;'>"
				+ "<h3>********** Helo World, Spring MVC Tutorial</h3>This message is coming from CrunchifyHelloWorld.java **********</div><br><br>";
		return new ModelAndView("welcome", "message", message);
	}
		
	@RequestMapping("/login_action")
	public String processLogin() {
		log.info(TAG + " /login_action btn clicked. Returning login view");			
		return "login";
	}
	
	@RequestMapping("/registration_action")
	public String processRegistration() {
		log.info(TAG + " /registration_action btn clicked. Returning login view");			
		return "registration";
	}
	
	@RequestMapping("/car_fuel_action")
	public String processCarFuel() {
		log.info(TAG + " /car_fuel_action btn clicked. Returning car-fuel view");
		return "car-fuel";
	}
	
}