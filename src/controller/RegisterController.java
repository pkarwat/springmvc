package controller;

import java.util.HashSet;
import java.util.Set;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import model.RoleDTO;
import model.ServerResponseDTO;
import model.ServerResponseDTO.RESPONSE_CODE;
import model.UserDTO;
import service.UserServiceImpl;

@Controller
@RequestMapping("/register")
public class RegisterController {
	
	private static final String TAG = RegisterController.class.getName();
	private static final Logger log = Logger.getLogger(RegisterController.class.getName());
	
	// --- Members ----------------------------------------------------------------------------------
	
	@Autowired
	UserServiceImpl userServiceImpl;
	
	// --- RestMethods ------------------------------------------------------------------------------

	 @RequestMapping(method=RequestMethod.POST)
	    public @ResponseBody ServerResponseDTO register(@RequestBody UserDTO u) {
	    	log.info(TAG + " POST");
	    	
	    	RoleDTO r = new RoleDTO();
	    	r.setId(RoleDTO.ROLE_ID.USER.getValue());
	    	
	    	HashSet<RoleDTO> s = new HashSet<RoleDTO>();
	    	s.add(r);
	    	
	    	//TODO wylapywac wyjatki przy dodawaniu do bazy, np gdy powtarza sie email
	    	// TODO wysylka maila z linkiem aktywacyjnym
	    	UserDTO userDTO = new UserDTO();
	    	userDTO.setEmail(u.getEmail());
	    	userDTO.setName(u.getName());
	    	userDTO.setPassword(u.getPassword());
	    	userDTO.setRoles(s);
	    	
	    	long id = userServiceImpl.add(userDTO);
	    	
	    	ServerResponseDTO response = new ServerResponseDTO();
	    	if (id > 0) {
	    		response.setCode(ServerResponseDTO.RESPONSE_CODE.SUCCESS.ordinal());
		    	response.setMsg("Zarejestrowano");	  	
	    	} else {
	    		response.setCode(ServerResponseDTO.RESPONSE_CODE.ERROR.ordinal());
	    		response.setMsg("Nie mo�na zarejestrowa�");
	    	}
	    	
	    	return response;
	    }   

}
