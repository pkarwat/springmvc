package controller;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import model.ServerResponseDTO;
import model.UserDTO;
import service.UserServiceImpl;

@Controller
@RequestMapping("/login")
public class LoginController {

	private static final String TAG = LoginController.class.getName();
	private static final Logger log = Logger.getLogger(RegisterController.class.getName());
	
	// --- Members -----------------------------------------------------------------
	
	@Autowired
	UserServiceImpl userServiceImpl;
	
	// --- RestMethods -------------------------------------------------------------
	
	@RequestMapping(method=RequestMethod.POST)
    public @ResponseBody ServerResponseDTO login(@RequestBody UserDTO u) {
        log.info(TAG + " POST");
        if (u != null) {
        	log.info("userDTO: " + u.toString());
        }

        UserDTO userDTO = userServiceImpl.getByCredentials(u.getEmail(), u.getPassword());
        userDTO.setRoles(null);//TODO fix

        ServerResponseDTO response = new ServerResponseDTO();
        response.setCode(ServerResponseDTO.RESPONSE_CODE.SUCCESS.ordinal());
        response.setMsg("Zalogowano");
        response.setItem(userDTO);

        return response;
    }
	
}
