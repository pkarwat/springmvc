package controller;

import java.util.logging.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import model.CarFuelDTO;

@Controller
@RequestMapping("/test-rest")
public class TestController {

	private static final String TAG = TestController.class.getName();	
	private static final Logger log = Logger.getLogger(TestController.class.getName());
	
	// --- Members -------------------------------------------------------------------------------------------
	
	@RequestMapping(method=RequestMethod.POST)
    public @ResponseBody String TestControllerPostMethod(@RequestBody CarFuelDTO c) {
    	log.info(TAG + " POST " + c.toString());    	
    	
    	return "TEST-REST OK";
    }
	
}
