package model;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ServerResponseListDTO<T> {

	private int code;
    private String msg;
    private List<T> items;
	
    public int getCode() {
		return code;
	}
	
    public void setCode(int code) {
		this.code = code;
	}
	
    public String getMsg() {
		return msg;
	}
	
    public void setMsg(String msg) {
		this.msg = msg;
	}
	
    public List<T> getItems() {
		return items;
	}
	
    public void setItems(List<T> items) {
		this.items = items;
	}      
	
    @Override
    public String toString() {
    	return ToStringBuilder.reflectionToString(this);
    }
    
}
