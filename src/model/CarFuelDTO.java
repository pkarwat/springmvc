package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "car_fuel")
public class CarFuelDTO {

	@Id
	@Column(nullable = false)		
	@GeneratedValue(strategy = GenerationType.IDENTITY)	//autoincrement
	private long id;
	
	@Column(name = "date")
	private Date date;
	
	@Column(name = "distance_traveled")
	private double distance_traveled;
	
	@Column(name = "price")	//precision=10, scale=2
	private double price;
	
	@Column(name = "capacity")
	private double capacity;
	
	@Column(name = "station", length = 255)
	private String station;
	
	@Column(name = "suser_id")
	private long suser_id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getDistance_traveled() {
		return distance_traveled;
	}

	public void setDistance_traveled(double distance_traveled) {
		this.distance_traveled = distance_traveled;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getCapacity() {
		return capacity;
	}

	public void setCapacity(double capacity) {
		this.capacity = capacity;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}
	
	public long getSuser_id() {
		return suser_id;
	}

	public void setSuser_id(long suser_id) {
		this.suser_id = suser_id;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
}
