package model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "role")
public class RoleDTO {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	
	@Column(name = "name")
	private String name;
	
	@ManyToMany(mappedBy = "roles")
	private Set<UserDTO> users;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Set<UserDTO> getUsers() {
		return users;
	}
	
	public void setUsers(Set<UserDTO> users) {
		this.users = users;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	// ---------------------------------------------------------------------------------------------------------
	
	public enum ROLE_ID {
		
		SUPER_ADMIN(1), 
	    ADMIN(2), 
	    USER(3);
	    
		private final int value;

	    private ROLE_ID(int value) {
	        this.value = value;
	    }

	    public int getValue() {
	        return value;
	    }	
	}
	
}
