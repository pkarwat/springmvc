package model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "suser")
public class UserDTO {

	@Id
	@Column(nullable = false)		
	@GeneratedValue(strategy = GenerationType.IDENTITY)	//autoincrement
	private long id;
	
	@Column(name = "email")	//, unique=true jak dziala?
	private String email;	

	@Column(name = "name")
	private String name;
	
	@Column(name = "password")
	private String password;
	
	@ManyToMany
	@JoinTable(name = "suser_role", 
		joinColumns = @JoinColumn(name = "suser_id"), 
		inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<RoleDTO> roles;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<RoleDTO> getRoles() {
		return roles;
	}

	public void setRoles(Set<RoleDTO> roles) {
		this.roles = roles;
	}			
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
}
