package model;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ServerResponseDTO<T> {

    private int code;
    private String msg;
    private T item;
	
    public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public T getItem() {
		return item;
	}
	public void setItem(T item) {
		this.item = item;
	}      
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	// --------------------------------------------------------------------------------------------------------
	
	public enum RESPONSE_CODE {
		ERROR,
	    SUCCESS 	
	}
	
}
