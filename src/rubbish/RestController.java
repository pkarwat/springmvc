/*package rubbish;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import model.GreetingDTO;
import model.UserDTO;
import service.CommonService;

@Controller
@RequestMapping("/hello-world")
public class RestController {

    private static final String template = "_Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    
    @Autowired
    CommonService<UserDTO> userServiceImpl;   
    
    @RequestMapping(method=RequestMethod.GET)
    public @ResponseBody GreetingDTO sayHello(@RequestParam(value="name", required=false, defaultValue="Stranger") String name) {
    	 	
        return new GreetingDTO(counter.incrementAndGet(), String.format(template, name));
    }
    
    @RequestMapping(method=RequestMethod.POST)
    public @ResponseBody GreetingDTO sayHelloPOST(@RequestParam(value="name", required=false, defaultValue="Stranger") String name) {
        return new GreetingDTO(counter.incrementAndGet(), String.format("POST METHOD ", name));
    }

}*/