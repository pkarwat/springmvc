package dao;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import model.UserDTO;

public class UserDAOImpl implements IUserdDAO {

	private static final String TAG = UserDAOImpl.class.getName();
	
	private static final Logger log = Logger.getLogger(UserDAOImpl.class.getName());
	
	// --- Members ----------------------------------------------------------------
	
	@Autowired
	SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	
	// --- IUserDAO ---------------------------------------------------------------
	
	@Transactional
	@Override
	public long add(UserDTO u) {
		log.info(TAG + " add(User u)");
		
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		//session.persist(s);
		session.save(u);
		tx.commit();
		
		Serializable id = session.getIdentifier(u);
		log.info(TAG + " new user id: " + id);
		
		session.close();
		return (Long)id;		
	}

	@Transactional
	@Override
	public UserDTO getByCredentials(String email, String password) {
		log.info(TAG + " getByCredentials(email, password) --> " + email + "/" + "password");
		
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		//TODO bug is here
		Query query = session.createQuery("from UserDTO where email = '" + email + "' and password = '" + password + "'");
		UserDTO u = (UserDTO) query.uniqueResult();
		
		log.info("Wyciągnięty user: " + u.toString());
		log.info("wyciagniete imie: " + u.getName());
		session.close();
		
		return u;
	}
	
}