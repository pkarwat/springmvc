package dao;

import java.util.List;

import model.CarFuelDTO;

public interface ICarFuelDAO {

	long add(CarFuelDTO c);
	
	CarFuelDTO getById(long id);

	CarFuelDTO getLast(long userId);
	
	CarFuelDTO update(CarFuelDTO c);	
	
	void delete(long id);
	
	List<CarFuelDTO> getAll(int page);

	List<CarFuelDTO> getAll(long userId);
	
}
