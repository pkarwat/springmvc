package dao;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import model.CarFuelDTO;

public class CarFuelDAOImpl implements ICarFuelDAO {

	private static final String TAG = CarFuelDAOImpl.class.getName();
	private static final Logger log = Logger.getLogger(CarFuelDAOImpl.class.getName());
	
	@Autowired
	SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional	
	@Override
	public long add(CarFuelDTO c) {
		log.info(TAG + " add()");
		
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		//session.persist(s);
		session.save(c);
		tx.commit();
		
		Serializable id = session.getIdentifier(c);
		log.info(TAG + " new car_fuel id: " + id);
		
		session.close();
		return (long) id;	
	}
	
	/**
	 * return null if does not exist
	 */
	@Override
	public CarFuelDTO getById(long id) {
		log.info(TAG + " getById(id) --> " + id);
		
		Session session = sessionFactory.openSession();
		CarFuelDTO c = (CarFuelDTO) session.get(CarFuelDTO.class, id);
		session.close();	
		
		return c;
	}

	@Override
	public CarFuelDTO getLast(long userId) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public CarFuelDTO update(CarFuelDTO c) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void delete(long id) {
		log.info(TAG + "delete(long id) --> " + id); 
		
		Session session = this.sessionFactory.openSession();
		Query query = session.createQuery("delete CarFuelDTO where id = " + id);
		 
		int result = query.executeUpdate();
		 
		if (result > 0) {
		    System.out.println("CarFuel was removed");
		}
		
	}	

	@Override
	public List<CarFuelDTO> getAll(int page) {
		log.info(TAG + " getAll()");
		
		int maxResults = 2;
		int firstResult = page*maxResults;	//pierwsza strona to strona 0
		
		Session session = this.sessionFactory.openSession();
		
		Query q = session.createQuery("from CarFuelDTO");
		q.setFirstResult(firstResult);
		q.setMaxResults(maxResults);
		
		List<CarFuelDTO> list = q.list();
		
		//List<CarFuelDTO> list = session.createQuery("from CarFuelDTO").list();
		
		for (int i=0; i<list.size(); i++) {
			log.info(TAG + " wynik: " + i + " " + list.get(i).toString());
		}
		
		session.close();
		return list;
	}

	@Override
	public List<CarFuelDTO> getAll(long userId) {
		log.info(TAG + " getAll(userId) -> " + userId);
		
		Session session = this.sessionFactory.openSession();
		//List<Student> list = (List<Student>)session.createQuery("from student").list();
		Query query = session.createQuery("from CarFuelDTO where suser_id = " + userId);
		List<CarFuelDTO> list = query.list();
		
		log.info(TAG + " list.size() = " + list.size());
		/*for (int i=0; i<list.size(); i++) {
			log.info(TAG + " " + i + " " + list.get(i).toString());
		}*/
		
		session.close();
		return list;
	}	

}
