package dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import model.UserDTO;

@Repository
public interface IUserdDAO {

	long add(UserDTO u);
	
	//void update(UserDTO u);
	
	//void getById(Long id);
	
	UserDTO getByCredentials(String email, String password);
	
	//List<UserDTO> getAll();
	
}
