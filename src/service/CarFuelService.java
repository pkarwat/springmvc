package service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;

import dao.ICarFuelDAO;
import model.CarFuelDTO;

public class CarFuelService implements ICarFuelDAO {

	private static final String TAG = CarFuelService.class.getName();
	private static final Logger log = Logger.getLogger(CarFuelService.class.getName());
	
	@Autowired
    ICarFuelDAO carFuelDAOImpl; 
	
	@Override
	public long add(CarFuelDTO c) {
		log.info(TAG + "add()");
		return carFuelDAOImpl.add(c);
	}
	
	@Override
	public CarFuelDTO getById(long id) {		
		return carFuelDAOImpl.getById(id);
	}

	@Override
	public CarFuelDTO getLast(long userId) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public CarFuelDTO update(CarFuelDTO c) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}	
	
	@Override
	public void delete(long id) {
		carFuelDAOImpl.delete(id);
	}

	@Override
	public List<CarFuelDTO> getAll(int page) {
		return carFuelDAOImpl.getAll(page);
	}

	@Override
	public List<CarFuelDTO> getAll(long userId) {
		return carFuelDAOImpl.getAll(userId);
	}

}
