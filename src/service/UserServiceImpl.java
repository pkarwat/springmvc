package service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;

import dao.IUserdDAO;
import model.UserDTO;

//public class UserServiceImpl implements CommonService<UserDTO> {
public class UserServiceImpl implements IUserdDAO {

	private static final String TAG = UserServiceImpl.class.getName();	
	private static final Logger log = Logger.getLogger(UserServiceImpl.class.getName());
	
	// --- Members ----------------------------------------------------------------
	
	@Autowired
    IUserdDAO userDAOImpl;
	
	// --- IUserDAO ----------------------------------------------------------------
	
	@Override
	public long add(UserDTO u) {
		return userDAOImpl.add(u);
	}

	@Override
	public UserDTO getByCredentials(String email, String password) {
		return userDAOImpl.getByCredentials(email, password);
	} 

}
