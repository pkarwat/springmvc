package service;

import java.util.List;

public interface CommonService<T extends Object> {
	
	public long add(T o);
	
	public List<T> getList();
	
	public T getRowById(long id);
	
	public long updateRow(T o);
	
	public long deleteRow(long id);

}
