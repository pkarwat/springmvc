<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
	<head>
		<h1 style="color:#FFFFFF;">Tankowania</h1>	
		<script src = "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
	</head>
	
	<!--  * * * * * * * * * * * * * * * * * * * *  V I E W * * * * * * * * * * * * * * * * * * * * -->

	<body bgcolor="#4F5B66" ng-app="mainApp" ng-controller="carFuelController">
		
		<h2 style="color:#FFFFFF;">Dodaj nowe tankowanie</h2>
		<hr>
		
		<div>
         	<p>Date: <input type = "text" ng-model = "carFuelDate"></p>
			<p>Distance: <input type = "text" ng-model = "carFuelDistanceTraveled"></p>
			<p>Price: <input type = "text" ng-model = "carFuelPrice"></p>
			<p>Capacity: <input type = "text" ng-model = "carFuelCapacity"></p>
			<p>Station: <input type = "text" ng-model = "carFuelStation"></p>

			<p align="center">
			<button ng-click="addCarFuel()">Dodaj</button>
			</p>
			
      	</div>
	      
	</body>

	<!--  * * * * * * * * * * * * * * * * * * * * C O N T R O L L E R * * * * * * * * * * * * * * * * * * * *  -->

	<script>
         var mainApp = angular.module("mainApp", []);
         
         mainApp.controller('carFuelController', function($scope, $http) {          
            $scope.addCarFuel = function() {
               //alert($scope.carFuelDate + " " + $scope.carFuelDistance + " " + $scope.carFuelPrice + " " + $scope.carFuelCapacity + " " + $scope.carFuelStation);                             	
               	
               	var carFuelDTO = {
               			date: $scope.carFuelDate,
               			distance_traveled: $scope.carFuelDistanceTraveled,
               			price: $scope.carFuelPrice,
               			capacity: $scope.carFuelCapacity,
               			station: $scope.carFuelStation,
               			suser_id: 1
               	};
               	
               	$http.post('/SpringMVC/car-fuel', carFuelDTO, {
	            	   headers: {
	            		   'Content-Type': 'application/json'
	            	      }	   
	               	})
	               .success(function() {
	                 console.log("msg sent");
	                 //alert("ok: " + $scope.email + ", password: " + $scope.password);
	                 alert("ok");
	               })
	               .error(function() {
	                 console.log("msg failed");
	                 //alert("fail: " + $scope.email + ", password: " + $scope.password);
	                 alert("fail");
	               });               	               	               	                      
            }
         });
      </script>


</html>