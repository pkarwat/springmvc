<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
	<head>
		<h2>Logowanie</h2>	
		<script src = "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
	</head>
	
	<!--  * * * * * * * * * * * * * * * * * * * *  V I E W * * * * * * * * * * * * * * * * * * * * -->

	<body ng-app="mainApp" ng-controller="loginController">
		
		<div>
         	<p>Enter your email: <input type = "text" ng-model = "email"></p>
			<p>Enter your password: <input type = "password" ng-model = "password"></p>

			<p align="center">
			<button ng-click="login()">Login</button>
			</p>

			<br><br>
			------------------------------------------------------
			<br>
			DEBUG

         	<p>email: <span ng-bind = "email"></span></p>
        	<p>password: {{password}}</p>
      	</div>
	      
	</body>

	<!--  * * * * * * * * * * * * * * * * * * * * C O N T R O L L E R * * * * * * * * * * * * * * * * * * * *  -->

	<script>
         var mainApp = angular.module("mainApp", []);
         
         mainApp.controller('loginController', function($scope, $http) {          
            $scope.login = function() {
               //alert("email: " + $scope.email + ", password: " + $scope.password);                             	
               	
               	var userDTO = {
               			email: $scope.email,
               			password: $scope.password
               	};
               	
              	//var carfuel = {station: $scope.email};
               	//$http.post('/SpringMVC/test-rest', carfuel, {
             	$http.post('/SpringMVC/login', userDTO, {
	            	   headers: {
	            		   'Content-Type': 'application/json'
	            	      }	   
	               	})
	               .success(function() {
	                 console.log("msg sent");
	                 alert("ok: " + $scope.email + ", password: " + $scope.password);
	               })
	               .error(function() {
	                 console.log("msg failed");
	                 alert("fail: " + $scope.email + ", password: " + $scope.password);
	               });
            }
         });
      </script>


</html>