--- PostgreSQL
--- za nazawa tabeli musi byc spacja
--- auto_increment --> serial

---------------------------------
DROP TABLE role;

CREATE TABLE role (
	id		SERIAL PRIMARY KEY,
    name 	varchar(45) DEFAULT NULL
);


INSERT INTO role (name) VALUES ('SUPER_ADMIN');
INSERT INTO role (name) VALUES ('ADMIN');
INSERT INTO role (name) VALUES ('USER');

SELECT * FROM ROLE;
---------------------------------
--- NIE MOZNA STWORZYC TABELI O NAZWIE USER
DROP TABLE suser cascade;

CREATE TABLE suser (
    id			    SERIAL PRIMARY KEY,
    email			VARCHAR(255) NOT NULL UNIQUE,
    name	        VARCHAR(255) DEFAULT NULL,
    password        VARCHAR(255) DEFAULT NULL
);


INSERT INTO suser (email, name, password) VALUES ('a@b', 'a', 11), ('b@b', 'b', 22), ('c@c', 'c', 33);

SELECT * FROM suser;
---------------------------------
DROP TABLE suser_role;

CREATE TABLE suser_role (
  	suser_id	int NOT NULL,
    role_id		int NOT NULL,
    PRIMARY KEY (suser_id, role_id),   
    
    CONSTRAINT fk_suser_role_suserid FOREIGN KEY (suser_id) REFERENCES suser (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_suser_role_roleid FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO suser_role (suser_id, role_id) VALUES (1, 1);
INSERT INTO suser_role (suser_id, role_id) VALUES (2, 2);
INSERT INTO suser_role (suser_id, role_id) VALUES (3, 3);
INSERT INTO suser_role (suser_id, role_id) VALUES (3, 2);

SELECT * FROM suser_role;

/*TODO:  
 *ON DELETE CASCADE ON UPDATE CASCADE, 
 *KEY `fk_user_role_roleid_idx` (`role_id`), 
 */
---------------------------------
DROP TABLE car_fuel;

CREATE TABLE car_fuel (
	id					SERIAL PRIMARY KEY,
    date 				TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    distance_traveled	DECIMAL(10, 2),
    price				DECIMAL(10, 2),
    capacity			DECIMAL(10, 2),
    station 			VARCHAR(255) DEFAULT NULL,
    suser_id			INTEGER REFERENCES suser (id)
);

INSERT INTO car_fuel (distance_traveled, price, capacity, station, suser_id) VALUES (180.567, 4.348, 11.252, 'Lukoil', 1);
INSERT INTO car_fuel (distance_traveled, price, capacity, station, suser_id) VALUES (181.567, 4.348, 11.252, 'Lukoil', 1);
INSERT INTO car_fuel (distance_traveled, price, capacity, station, suser_id) VALUES (182.567, 4.348, 11.252, 'Lukoil', 2);

SELECT * FROM car_fuel;